import React, { useState } from 'react';
import Navbar from './Compenents/Navbar/Navbar';
import Home from './Compenents/Home/Home';
import President from './Compenents/President/President';
import Title from './Compenents/Title/Title';
import Translations from './Compenents/Translation.json';
import About from './Compenents/About/About';
import Terrain from './Compenents/Terrain/Terrain';
import Carte from './Compenents/Carte/Carte';
import Contact from './Compenents/Contact/Contact';
import Footer from './Compenents/Footer/Footer';
import VideoPlayer from './Compenents/VideoPlayer/VideoPlayer';
import Formulaire from './Compenents/Formulaire/Formulaire';
import { Link } from 'react-scroll'; // Importer Link depuis react-scroll
import './App.css';

const App = () => {
    const [language, setLanguage] = useState('fr');
    const [playState, setPlayState] = useState(false);

    const handleLanguageChange = (selectedLanguage) => {
        setLanguage(selectedLanguage);
    };

    // Déterminez la classe CSS en fonction de la langue sélectionnée
    const textAlignmentClass = language === 'ar' ? 'rtl' : 'ltr';

    return (
        <div className={`App ${textAlignmentClass}`}>
            <Navbar onLanguageChange={handleLanguageChange} />
            <Home language={language} />
            <div className='container'>
                <Title sousTitre={Translations.subtitle[language]} titre={Translations.title[language]}/>
                <President language={language}/>
            </div>
            <div>
                <Title sousTitre={Translations.subtitle1[language]} titre={Translations.title1[language]} />
                <About language={language} setPlayState={setPlayState}/>
            </div>
            <div>
                <Title sousTitre={Translations.subtitle2[language]} titre={Translations.title2[language]} />
                <Terrain language={language}/>
            </div>
            <div>
                <Title sousTitre={Translations.subtitle3[language]} titre={Translations.title3[language]} />
                <Carte/>
            </div>
            <div>
                <Title sousTitre={Translations.subtitle4[language]} titre={Translations.title4[language]} />
                <Contact language={language}/>
            </div>
            <div id="formulaire"> 
            <Title sousTitre={Translations.subtitle5[language]} titre={Translations.title5[language]} />
             <Formulaire language={language}/>
            </div>
            <Footer language={language}/>
            <VideoPlayer playState={playState} setPlayState={setPlayState}/>
        </div>
    );
}

export default App;
