import React, { useEffect,useState } from 'react';
import './Navbar.css';
import logo from '../../assets/logo.png';
import Translations from '../Translation.json';
import { Link } from 'react-scroll';
import menu from '../../assets/menu.png';

const Navbar = ({ onLanguageChange }) => {
    const [langue, setLangue] = useState('fr');
    const changerLangue = (event) => {
        const selectedLanguage = event.target.value;
        setLangue(selectedLanguage);
        onLanguageChange(selectedLanguage);
    };
    const[sticky, setSticky] = useState(false);
    useEffect(()=>{
        window.addEventListener('scroll',()=>{
            window.scrollY > 500 ? setSticky(true) : setSticky(false);
        })
    },[]);
    const[mobileMenu, setMobileMenu] = useState(false)
    const toggleMenu =()=>{
        mobileMenu ? setMobileMenu(false) : setMobileMenu(true);
    }

    return (
        <nav className={`container ${sticky? 'dark-nav' : ''}`}>
            <div className="logo-container">
                <img src={logo} alt="" className='logo' />
            </div>
            <ul className={mobileMenu ? '':'hide-mobile-menu'}>
                <li><Link to='home' smooth={true} offset={0} duration={500}>{Translations.accueil[langue]}</Link></li>
                <li><Link to='president' smooth={true} offset={0} duration={500}>{Translations.president[langue]}</Link></li>
                <li><Link to='about' smooth={true} offset={0} duration={500}>{Translations.a_propos[langue]}</Link></li>
                <li><Link to='terrain' smooth={true} offset={0} duration={500}>{Translations.terrain[langue]}</Link></li>
                <li><Link to='carte' smooth={true} offset={0} duration={500}>{Translations.carte[langue]}</Link></li>
                <li><Link to='contact' smooth={true} offset={0} duration={500}>{Translations.contact[langue]}</Link></li>
            </ul>
            <select value={langue} onChange={changerLangue} className='select'>
                <option value="fr">Français</option>
                <option value="ar">العربية</option>
            </select>
            <img src={menu} alt='' className='menu' onClick={toggleMenu}/>
        </nav>
    );
}

export default Navbar;
