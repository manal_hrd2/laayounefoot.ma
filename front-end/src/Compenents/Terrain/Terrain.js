import React from "react";
import './Terrain.css';
import { useRef } from "react";
import Translations from '../Translation.json';
import right from '../../assets/right.png';
import left from '../../assets/left.png';
import alwifaq from '../../assets/alwifaq.png';
import arraha from '../../assets/arraha.png';
import diridak from '../../assets/diridak.png';
import ibnZohr from '../../assets/ibnZohr.png';
import moulayR from '../../assets/moulayR.png';
import location from '../../assets/location.png';

const Terrain = ({ language }) => {
    const slider = useRef ();
    let tx = 0;

    const slideForward = ()=>{
        if(tx > -50){
            tx -= 25;
        }
        slider.current.style.transform = `translateX(${tx}%)`;
    }
    const slideBackward = ()=>{
        if(tx < 0){
            tx += 25;
        }
        slider.current.style.transform = `translateX(${tx}%)`;
        
    }

    return(
        <div className="terrain">
            <div className="slider">
                <ul ref={slider}>
                    <li>
                        <div className="slide">
                          <img src={alwifaq} alt=""></img>  
                          <div className="caption">
                          <img src={location} alt=""></img>
                            <p>{Translations.terrain1[language]}</p>
                          </div>
                        </div>
                    </li>
                    <li>
                        <div className="slide">
                          <img src={arraha} alt=""></img>
                          <div className="caption">
                            <img src={location} alt=""></img>
                            <p>{Translations.terrain2[language]}</p>
                          </div>  
                        </div>
                    </li>
                    <li>
                        <div className="slide">
                          <img src={diridak} alt=""></img> 
                          <div className="caption">
                          <img src={location} alt=""></img>
                            <p>{Translations.terrain3[language]}</p>
                          </div> 
                        </div>
                    </li>
                    <li>
                        <div className="slide">
                          <img src={ibnZohr} alt=""></img> 
                          <div className="caption">
                          <img src={location} alt=""></img>
                            <p>{Translations.terrain4[language]}</p>
                          </div> 
                        </div>
                    </li>
                    <li>
                        <div className="slide">
                          <img src={moulayR} alt=""></img>  
                          <div className="caption">
                          <img src={location} alt=""></img>
                            <p>{Translations.terrain5[language]}</p>
                          </div>
                        </div>
                    </li>
                </ul>
            </div>
            <img src={left} alt="" className="left-btn" onClick={slideForward} />
            <img src={right} alt="" className="right-btn" onClick={slideBackward} />
        </div>
    )
}
export default Terrain;