import React, { useState } from "react";
import './Home.css';
import Translations from '../Translation.json';
import greenArrow from '../../assets/greenArrow.png';
import { Link } from "react-scroll";


const Home = ({ language }) => {
    return (
        <div className="home container">
            <div className="home-text">
                <h1>{Translations.bienvenue[language]} </h1>
               <button type="button" className="btn"><Link to='formulaire' smooth={true} offset={0} duration={500} >{Translations.s_inscrire[language]}</Link><img src={greenArrow} alt="" /></button>
            </div>
        </div>
    );
}
export default Home;