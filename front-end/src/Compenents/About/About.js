import React from "react";
import './About.css';
import Translations from '../Translation.json';
import about from '../../assets/about.png';
import play from '../../assets/play.png'

const About = ({ language, setPlayState }) => {
    return(
        <div className="about">
            <div className="about-left">
                <p>{Translations.txt[language]}</p>
                <h4>{Translations.txt2[language]}</h4>
            </div>
            <div className="about-right">
                <img src={about} alt="" className="about-img"/>
                <img src={play} alt="" className="play-icon" onClick={() => { setPlayState(true) }}/>
            </div>
        </div>
    );
};
export default About;