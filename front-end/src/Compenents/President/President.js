import React from "react";
import './President.css';
import  { useState } from "react";
import Translations from '../Translation.json';
import president from '../../assets/president.png';

const President = ({ language }) => {
    return(
        <div className="president">
            <div className="president-left">
                <img src={president} alt="" className="president-img"/>
            </div>
            <div className="president-right">
                <h3>{Translations.hello[language]}</h3>
                <p>"{Translations.president1[language]}"</p>
                <h4>"{Translations.mot[language]}"</h4>
            </div>


        </div>
    )
}
export default President