import React, { useState } from 'react';
import './Formulaire.css';
import Translations from '../Translation.json';
import axios from 'axios';

function Formulaire({ language }) {
  const [formData, setFormData] = useState({
    nom: '',
    prenom: '',
    dateNaissance: '',
    lieuNaissance: '',
    nomPere: '',
    prenomPere: '',
    cin: '',
    telephone: '',
    terrain: '',
    dateDebut: '',
    dateFin: '',
    nomAgence: '',
  });

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    console.log(formData);
    try {
      const res = await axios.post('http://localhost:8000/api/joueur', formData);
      console.log(res.data);
      if (res.data.status === 200) {
        console.log(res.data.message);
        setFormData({
          nom: '',
          prenom: '',
          dateNaissance: '',
          lieuNaissance: '',
          nomPere: '',
          prenomPere: '',
          cin: '',
          telephone: '',
          terrain: '',
          dateDebut: '',
          dateFin: '',
          nomAgence: '',
        });
      }
    } catch (error) {
      console.error('Erreur lors de l\'envoi des données:', error);
    }
  };

  return (
    <div className="main-container">
      <form className="form" onSubmit={handleSubmit}>
        <fieldset>
          <legend>{Translations.form1[language]}</legend>
          <label>
            {Translations.form2[language]}
            <input type="text" name="nom" value={formData.nom} onChange={handleChange} />
          </label>
          <br />
          <label>
            {Translations.form3[language]}
            <input type="text" name="prenom" value={formData.prenom} onChange={handleChange} />
          </label>
          <br />
          <label>
            {Translations.form4[language]}
            <input type="date" name="dateNaissance" value={formData.dateNaissance} onChange={handleChange} />
          </label>
          <br />
          <label>
            {Translations.form5[language]}
            <input type="text" name="lieuNaissance" value={formData.lieuNaissance} onChange={handleChange} />
          </label>
          <br />
        </fieldset>
        <fieldset>
          <legend>{Translations.form9[language]}</legend>
          <label>
            {Translations.form10[language]}
            <input type="text" name="nomPere" value={formData.nomPere} onChange={handleChange} />
          </label>
          <br />
          <label>
            {Translations.form11[language]}
            <input type="text" name="prenomPere" value={formData.prenomPere} onChange={handleChange} />
          </label>
          <br />
          <label>
            {Translations.form12[language]}
            <input type="text" name="cin" value={formData.cin} onChange={handleChange} />
          </label>
          <br />
          <label>
            {Translations.form30[language]}
            <input type="text" name="telephone" value={formData.telephone} onChange={handleChange} />
          </label>
          <br />
        </fieldset>
        <fieldset>
          <legend>{Translations.form13[language]}</legend>
          <label>
            {Translations.form14[language]}
            <select className='select-terrain' name="terrain" value={formData.terrain} onChange={handleChange}>
              <option value="" disabled hidden>{Translations.form15[language]}</option>
              <option value="Terrain moulay rachid">{Translations.form16[language]}</option>
              <option value="Terrain Arraha">{Translations.form17[language]}</option>
              <option value="Terrain 84">{Translations.form18[language]}</option>
              <option value="Terrain Annahda">{Translations.form19[language]}</option>
              <option value="Terrain Alwifaq">{Translations.form20[language]}</option>
              <option value="Terrain Ibn zohr">{Translations.form21[language]}</option>
              <option value="Terrain Diridak">{Translations.form22[language]}</option>
              <option value="Terrain Alwahda">{Translations.form23[language]}</option>
              <option value="Terrain Hay alqassam">{Translations.form24[language]}</option>
            </select>
          </label>
          <br />
        </fieldset>
        <fieldset>
          <legend>{Translations.form25[language]}</legend>
          <label>
            {Translations.form26[language]}
            <input type="date" name="dateDebut" value={formData.dateDebut} onChange={handleChange} />
          </label>
          <br />
          <label>
            {Translations.form27[language]}
            <input type="date" name="dateFin" value={formData.dateFin} onChange={handleChange} />
          </label>
          <br />
          <label>
            {Translations.form28[language]}
            <input type="text" name="nomAgence" value={formData.nomAgence} onChange={handleChange} />
          </label>
          <br />
        </fieldset>
        <button type="submit">{Translations.form29[language]}</button>
      </form>
    </div>
  );
}

export default Formulaire;
