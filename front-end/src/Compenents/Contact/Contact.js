import React from "react";
import './Contact.css';
import Translations from '../Translation.json';
import email from '../../assets/email.png';
import web from '../../assets/web.png';
import lettre from '../../assets/lettre.png';
import telephone from '../../assets/telephone.png';



const Contact =({language})=>{

    const [result, setResult] = React.useState("");

    const onSubmit = async (event) => {
      event.preventDefault();
      setResult("Sending....");
      const formData = new FormData(event.target);
      formData.append("access_key", "b55d08e2-4f79-4682-903a-431316ebd8d8");
      const response = await fetch("https://api.web3forms.com/submit", {
        method: "POST",
        body: formData
      });
  
      const data = await response.json();
  
      if (data.success) {
        setResult("Form Submitted Successfully");
        event.target.reset();
      } else {
        console.log("Error", data);
        setResult(data.message);
      }
    };

    return (
        <div className="contact">
            <div className="contact-col">
                <h3>{Translations.cont1[language]} <img src={email} alt=""></img> </h3>
                <p>{Translations.cont2[language]}</p>
                <ul>
                    <li><img src={web} alt=""></img>Footlaayoune@gmail.com</li>
                    <li><img src={telephone} alt=""></img>05-76-12-34-00</li>
                    <li><img src={lettre} alt=""></img>Footlaayoune.ma</li>
                </ul>

            </div>
            <div className="contact-col">
                <form onSubmit={onSubmit}>
                    <label>{Translations.cont3[language]}</label>
                    <input type="text" name='name' placeholder={Translations.cont4[language]} required />
                    <label>{Translations.cont5[language]}</label>
                    <input type="text" name='name' placeholder={Translations.cont6[language]} required />
                    <label>{Translations.cont7[language]}</label>
                    <input type="tel" name="phone" placeholder={Translations.cont8[language]} required />
                    <label>{Translations.cont9[language]}</label>
                    <textarea name="message" rows="6" placeholder={Translations.cont10[language]} required/>
                    <button type="submit" className="btn-cont">{Translations.cont11[language]}</button>
                </form>
                <span>{result}</span>
            </div>

        </div>
    )
}
export default Contact;


