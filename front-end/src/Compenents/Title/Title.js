import React from "react";
import './Title.css';



const Title = ({ sousTitre , titre}) => {
    return ( 
        <div className="title">
            <p>{sousTitre}</p>
            <h2>{titre}</h2>
        </div>
    )
}
export default Title;