import React from "react";
import './Footer.css';
import Translations from '../Translation.json';

const Footer = ({language})=> {
    return (
        <div className="footer">
            <p>{Translations.foot1[language]}</p>
            <ul>
                <li>{Translations.foot2[language]}</li>
                <li>{Translations.foot3[language]}</li>
            </ul>
        </div>
    )
}
export default Footer;