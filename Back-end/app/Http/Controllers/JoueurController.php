<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Joueur;

class JoueurController extends Controller
{
    public function destroy(Joueur $joueur)
{
    $joueur->delete();

    return redirect()->route('dashboard')->with('success', 'Joueur supprimé avec succès');
}
    public function index()
    {
        $joueurs = Joueur::all(); // Récupérer tous les joueurs
        return view('dashboard', compact('joueurs'));
    }
    public function store(Request $request)
    {
        $joueur = new Joueur;
        $joueur->nom = $request->input('nom');
        $joueur->prenom = $request->input('prenom');
        $joueur->dateNaissance = $request->input('dateNaissance');
        $joueur->lieuNaissance = $request->input('lieuNaissance');
        $joueur->nomPere = $request->input('nomPere');
        $joueur->prenomPere = $request->input('prenomPere');
        $joueur->cin = $request->input('cin');
        $joueur->telephone = $request->input('telephone');
        $joueur->terrain = $request->input('terrain');
        $joueur->dateDebut = $request->input('dateDebut');
        $joueur->dateFin = $request->input('dateFin');
        $joueur->nomAgence = $request->input('nomAgence');
        $joueur->save();

        return response()->json([
            'status' => 200,
            'message' => 'Informations enregistrées avec succès',
        ]);
    }
}
