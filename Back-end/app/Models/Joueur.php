<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Joueur extends Model
{
    use HasFactory;
    protected $table = 'joueurs';
    protected $fillable = [
        'nom',
        'prenom',
        'dateNaissance',
        'lieuNaissance',
        'nomPere',
        'prenomPere',
        'cin',
        'telephone',
        'terrain',
        'dateDebut',
        'dateFin',
        'nomAgence',
    ];
}
