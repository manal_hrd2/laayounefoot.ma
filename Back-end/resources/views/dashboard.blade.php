<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('css/dashboard.css') }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
</head>
<body>
    
    <div>
        @include('layouts.navigation')

        <!-- Page Heading -->
        @if (isset($header))
            <header>
                <h2>{{ __('Dashboard') }}</h2>
            </header>
        @endif

        <!-- Page Content -->
        <main>
            <div>
                <p>Dashboard</p> <!-- Titre ajouté -->
                <div>
                
                    <table>
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nom</th>
                                <th>Prénom</th>
                                <th>Date de Naissance</th>
                                <th>Lieu de Naissance</th>
                                <th>Nom du Père</th>
                                <th>Prénom du Père</th>
                                <th>CIN</th>
                                <th>Téléphone</th>
                                <th>Terrain</th>
                                <th>Date de Début Assurence</th>
                                <th>Date de Fin Assurence</th>
                                <th>Nom de l'Agence</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($joueurs as $joueur)
                                <tr>
                                    <td>{{ $joueur->id }}</td>
                                    <td>{{ $joueur->nom }}</td>
                                    <td>{{ $joueur->prenom }}</td>
                                    <td>{{ $joueur->dateNaissance }}</td>
                                    <td>{{ $joueur->lieuNaissance }}</td>
                                    <td>{{ $joueur->nomPere }}</td>
                                    <td>{{ $joueur->prenomPere }}</td>
                                    <td>{{ $joueur->cin }}</td>
                                    <td>{{ $joueur->telephone }}</td>
                                    <td>{{ $joueur->terrain }}</td>
                                    <td>{{ $joueur->dateDebut }}</td>
                                    <td>{{ $joueur->dateFin }}</td>
                                    <td>{{ $joueur->nomAgence }}</td>
                                    <td>
                                        <form action="{{ route('joueurs.destroy', $joueur) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit">Supprimer</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </main>
    </div>
</body>
</html>
