<link rel="stylesheet" href="{{ asset('css/register.css') }}">
<div class='nav'>
    <img src="{{ asset('lg.png') }}" alt="Logo">
</div>
<form method="POST" action="{{ route('register') }}">
    @csrf

    <!-- Name -->
    <div>
        <label for="name">{{ __('Name') }}</label>
        <input id="name" type="text" name="name" :value="old('name')" required autofocus autocomplete="name" />
        <x-input-error :messages="$errors->get('name')" />
    </div>

    <!-- Email Address -->
    <div>
        <label for="email">{{ __('Email') }}</label>
        <input id="email" type="email" name="email" :value="old('email')" required autocomplete="username" />
        <x-input-error :messages="$errors->get('email')" />
    </div>

    <!-- Password -->
    <div>
        <label for="password">{{ __('Password') }}</label>
        <input id="password" type="password" name="password" required autocomplete="new-password" />
        <x-input-error :messages="$errors->get('password')" />
    </div>

    <!-- Confirm Password -->
    <div>
        <label for="password_confirmation">{{ __('Confirm Password') }}</label>
        <input id="password_confirmation" type="password" name="password_confirmation" required autocomplete="new-password" />
        <x-input-error :messages="$errors->get('password_confirmation')" />
    </div>

    <div>
        <a href="{{ route('login') }}">{{ __('Already registered?') }}</a>
        <button type="submit">{{ __('Register') }}</button>
    </div>
</form>
