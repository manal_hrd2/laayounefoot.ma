<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Navigation</title>
    <link rel="stylesheet" href="{{ asset('css/navigation.css') }}">
</head>
<body>
<nav class="navbar">
    <!-- Primary Navigation Menu -->
    <div class="container">
        <div class="nav-content">
            <div class="logo-and-links">
                <!-- Logo -->
                <div class="logo">
                    <a href="{{ route('dashboard') }}">
                        <img src="{{ asset('lg.png') }}" alt="Logo" class="logo-img">
                    </a>
                </div>

                <!-- Navigation Links -->
               
            </div>

            <!-- Settings Dropdown -->
            <div class="dropdown">
                <button class="dropdown-toggle">
                    <div>{{ Auth::user()->name }}</div>
                    <div class="dropdown-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                            <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 111.414 1.414l-4 4a1 1 01-1.414 0l-4-4a1 1 010-1.414z" clip-rule="evenodd" />
                        </svg>
                    </div>
                </button>
                <div class="dropdown-menu">
                    <a href="{{ route('profile.edit') }}">
                        {{ __('Profile') }}
                    </a>
                    <!-- Authentication -->
                    <form method="POST" action="{{ route('logout') }}">
                        @csrf
                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); this.closest('form').submit();">
                            {{ __('Log Out') }}
                        </a>
                    </form>
                </div>
            </div>

            <!-- Hamburger -->
            <div class="hamburger">
                <button @click="open = ! open">
                    <svg viewBox="0 0 24 24">
                        <path :class="{'hidden': open, 'inline-flex': ! open }" class="inline-flex" d="M4 6h16M4 12h16M4 18h16" />
                        <path :class="{'hidden': ! open, 'inline-flex': open }" class="hidden" d="M6 18L18 6M6 6l12 12" />
                    </svg>
                </button>
            </div>
        </div>
    </div>

    <!-- Responsive Navigation Menu -->
    <div :class="{'block': open, 'hidden': ! open}" class="responsive-menu">
        <div>
            <a href="{{ route('dashboard') }}" class="{{ request()->routeIs('dashboard') ? 'active' : '' }}">
                {{ __('Dashboard') }}
            </a>
        </div>

        <!-- Responsive Settings Options -->
        <div class="responsive-settings">
            <div>
                <div>{{ Auth::user()->name }}</div>
                <div>{{ Auth::user()->email }}</div>
            </div>

            <div>
                <a href="{{ route('profile.edit') }}">
                    {{ __('Profile') }}
                </a>
                <!-- Authentication -->
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); this.closest('form').submit();">
                        {{ __('Log Out') }}
                    </a>
                </form>
            </div>
        </div>
    </div>
</nav>
</body>
</html>
